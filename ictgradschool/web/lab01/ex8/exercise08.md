A demonstration of merge conflicts
----------------------------------

In Git, "merging" is the act of integrating another branch into your current working 
branch. You're taking changes from another context (that's what a branch effectively is: 
a context) and combine them with your current working files.

A great thing about having Git as your version control system is that it makes merging 
extremely easy: in most cases, Git will figure out how to integrate new changes.

However, there's a handful of situations where you might have to step in and tell Git 
what to do. Most notably, this is when changing the same file. Even in this case, Git 
will most likely be able to figure it out on its own. But if two people changed the 
same lines in that same file, or if one person decided to delete it while the other 
person decided to modify it, Git simply cannot know what is correct. Git will then 
mark the file as having a conflict - which you'll have to solve before you can continue 
your work.

Description obtained from the Git Tower book, found [here](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts)

Solving merge conflicts
-----------------------

Describe how to solve conflicts

How do we feel about merging
----------------------------

An opinion here
Vision 1