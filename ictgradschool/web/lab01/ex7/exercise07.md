###Herbivorous:
* Horse
* Cow
* Sheep
* Capybara
* Chicken

###Carnivorous:
* Polar Bear
* Hawk
* Lion
* Fox
* Frog

###Omnivorous:
* Cat
* Dog
* Rat
* Raccoon
* Fennec Fox